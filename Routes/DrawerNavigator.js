import React from 'react';
import Login from '../Screens/Login';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Message from '../Screens/Message';
import Details from '../Screens/Details';
import Calendar from '../Screens/Calendar';
import TabNaviagtor from './TabNavigtor';
import StackNavigator from './StackNavigator';

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName={Login}
      screenOptions={{headerShown: false}}>
      <Drawer.Screen name="Tab" component={TabNaviagtor} />
      <Drawer.Screen name="Stack" component={StackNavigator} />
      <Drawer.Screen name="login" component={Login} />
      <Drawer.Screen name="message" component={Message} />
      <Drawer.Screen name="details" component={Details} />
      <Drawer.Screen name="calendar" component={Calendar} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;

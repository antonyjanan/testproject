import React from 'react';
import SplashScreen from '../Screens/SplashScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from '../Screens/Login';
import RegisterScreen from '../Screens/RegisterScreen';
import Calendar from '../Screens/Calendar';
import TabNaviagtor from './TabNavigtor';
import DrawerNavigator from './DrawerNavigator';

const AuthStack = createNativeStackNavigator();
const StackNavigator = () => {
  return (
    <AuthStack.Navigator
      initialRouteName={SplashScreen}
      screenOptions={{headerShown: false}}>
      <AuthStack.Screen name="SplashScreen" component={SplashScreen} />
      <AuthStack.Screen name="login" component={Login} />
      <AuthStack.Screen name="calendar" component={Calendar} />
      <AuthStack.Screen name="register" component={RegisterScreen} />
    </AuthStack.Navigator>
  );
};

export default StackNavigator;
// Normal Function Navigation
// import {createStackNavigator} from 'react-navigation-stack';
// import {createAppContainer} from 'react-navigation';
// import Login from '../Screens/Login';
// import Details from '../Screens/Details';
// import Calendar from '../Screens/Calendar';
// import Message from '../Screens/Message';
// import RegisterScreen from '../Screens/RegisterScreen';
// import SplashScreen from '../Screens/SplashScreen';

// const StackNavigator = createStackNavigator(
//   {
//     SplashScreen: {
//       screen: SplashScreen,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     calendar: {
//       screen: Calendar,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     details: {
//       screen: Details,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     message: {
//       screen: Message,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     login: {
//       screen: Login,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//     register: {
//       screen: RegisterScreen,
//       navigationOptions: {
//         headerShown: false,
//       },
//     },
//   },
//   {
//     initialRouteName: 'SplashScreen',
//   },
// );

// export default createAppContainer(StackNavigator);
// Arrow function

// import * as React from 'react';
// import Login from '../Screens/Login';
// import Details from '../Screens/Details';
// import Calender from '../Screens/Calendar';
// import Message from '../Screens/Message';
// import {createNativeStackNavigator} from '@react-navigation/native-stack';

// const Stack = createNativeStackNavigator();
// const StackNavigator = () => {
//   return (
//     <Stack.Navigator
//       initialRouteName="login"
//       screenOptions={{headerShown: false}}>
//       <Stack.Screen name="login" component={Login} />
//       <Stack.Screen name="details" component={Details} />
//       <Stack.Screen name="calender" component={Calender} />
//       <Stack.Screen name="message" component={Message} />
//     </Stack.Navigator>
//   );
// };

// export default StackNavigator;

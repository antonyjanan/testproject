import React from 'react';
import {View, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Calendar from '../Screens/Calendar';
import Login from '../Screens/Login';
import Details from '../Screens/Details';
import Message from '../Screens/Message';
import {NavigationContainer} from '@react-navigation/native';
import RegisterScreen from '../Screens/RegisterScreen';
import SplashScreen from '../Screens/SplashScreen';

const BottomTab = createBottomTabNavigator();
const TabNaviagtor = () => {
  return (
    <BottomTab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarActiveTintColor: '#0284ff',
      }}
      initialRouteName="calendar">
      <BottomTab.Screen
        options={{
          tabBarStyle: {display: 'none'},
          tabBarIcon: ({focused}) =>
            focused ? (
              <View>
                <Image
                  source={require('../Screens/Image/colourman.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ) : (
              <View>
                <Image
                  source={require('../Screens/Image/man.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ),
        }}
        name="login"
        component={Login}
      />
      <BottomTab.Screen
        options={{
          tabBarIcon: ({focused}) =>
            focused ? (
              <View>
                <Image
                  source={require('../Screens/Image/calendar.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ) : (
              <View>
                <Image
                  source={require('../Screens/Image/calender.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ),
        }}
        name="calendar"
        component={Calendar}
      />
      <BottomTab.Screen
        options={{
          tabBarIcon: ({focused}) =>
            focused ? (
              <View>
                <Image
                  source={require('../Screens/Image/chaticon.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ) : (
              <View>
                <Image
                  source={require('../Screens/Image/chat.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ),
        }}
        name="message"
        component={Message}
      />
      <BottomTab.Screen
        options={{
          tabBarStyle: {display: 'none'},
          tabBarIcon: ({focused}) =>
            focused ? (
              <View>
                <Image
                  source={require('../Screens/Image/pad.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ) : (
              <View>
                <Image
                  source={require('../Screens/Image/notepad.png')}
                  resizeMod="contain"
                  style={{
                    width: 50,
                    height: 50,
                  }}
                />
              </View>
            ),
        }}
        name="details"
        component={Details}
      />
    </BottomTab.Navigator>
  );
};
export default TabNaviagtor;

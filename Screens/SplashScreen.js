import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

const SplashScreen = ({navigation}) => (
  <View style={styles.container}>
    <StatusBar backgroundColor="skyblue" barStyle="dark-content" />
    <View style={styles.header}>
      <Image
        source={require('../src/assets/company.png')}
        style={styles.logo}
      />
    </View>
    <View style={styles.footer}>
      <Text style={styles.title}>Stay connected with everyone!</Text>
      <Text style={styles.text}> Sign in with Account</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          onPress={() => navigation.navigate('login')}
          style={styles.button}>
          <Text style={styles.textSign}>Get Started</Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
);
const {height} = Dimensions.get('screen');
const Brand = height * 0.23;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#edf3fb',
    backgroundColor: 'skyblue',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    marginHorizontal: 10,
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  logo: {
    height: Brand,
    width: Brand,
    borderRadius: 100,
  },
  title: {
    marginTop: 30,
    marginLeft: 20,
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
    marginLeft: 20,
  },
  button: {
    borderRadius: 50,
    height: 40,
    width: 100,
    backgroundColor: 'skyblue',
    alignItems: 'center',
    marginTop: 10,
    // marginRight: 10,
  },
  buttonContainer: {
    alignItems: 'flex-end',
    marginRight: 30,
    // backgroundColor: 'red',
  },
  textSign: {
    marginTop: 10,
    justifyContent: 'center',
    color: 'blue',
    fontWeight: 'bold',
  },
});

export default SplashScreen;

import React, {useContext, useState, useEffect} from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  TextInput,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  Pressable,
  Linking,
  Alert,
  BackHandler,
} from 'react-native';
import {AuthContext} from '../src/context/AuthContext';

const Login = ({navigation}) => {
  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to go back?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const {signIn} = useContext(AuthContext);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <StatusBar backgroundColor="#edf3fb" barStyle="dark-content" />
        <View style={styles.header}>
          <Image source={require('./Image/a.png')} style={styles.Brand} />
        </View>
        <View style={styles.footer}>
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="Email ID"
              value={username}
              setValue={setUsername}
              style={styles.emailInputStyle}
              autoCapitalize="none"
            />
            <TextInput
              secureTextEntry={true}
              placeholder="password"
              value={password}
              setvalue={setPassword}
              style={styles.passwordInputStyle}
            />
            <TouchableOpacity>
              <View style={styles.forgetPassword}>
                <Text style={styles.paragragh}>Forget Password?</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <Pressable
              style={styles.button}
              onPress={() => {
                // navigation.navigate('calendar');
                signIn();
              }}>
              <Text style={styles.login}>Login</Text>
            </Pressable>
          </View>
          <View style={styles.createAccount}>
            <TouchableOpacity onPress={() => navigation.navigate('register')}>
              <Text style={styles.create}>create an account </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.orContainer}>
            <View style={styles.line} />
            <Text style={styles.orText}>OR</Text>
            <View style={styles.line} />
          </View>
          <View style={styles.connectContainer}>
            <Text style={styles.connect}>connect</Text>
          </View>
          <View style={styles.logo}>
            <TouchableOpacity onPress={() => Linking.openURL(MY_URL)}>
              <Image source={require('./Image/fb.png')} />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const MY_URL =
  'https://www.facebook.com/campaign/landing.php?campaign_id=14884913640&extra_1=s%7Cc%7C589460569900%7Cb%7Cfb%20login%27%7C&placement=&creative=589460569900&keyword=fb%20login%27&partner_id=googlesem&extra_2=campaignid%3D14884913640%26adgroupid%3D128696221912%26matchtype%3Db%26network%3Dg%26source%3Dnotmobile%26search_or_content%3Ds%26device%3Dc%26devicemodel%3D%26adposition%3D%26target%3D%26targetid%3Dkwd-370844460174%26loc_physical_ms%3D1007779%26loc_interest_ms%3D%26feeditemid%3D%26param1%3D%26param2%3D&gclid=EAIaIQobChMIjMCRo8Hz-QIVZJNmAh1wIQg2EAAYASAAEgJtkPD_BwE';
const {height} = Dimensions.get('screen');
const Brand = height * 0.23;

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#edf3fb',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 82,
    paddingHorizontal: 30,
    elevation: 35,
    marginHorizontal: 31,
  },
  Brand: {
    width: Brand,
    height: Brand,
  },
  button: {
    width: '100%',
    height: 45,
    borderRadius: 20,
    backgroundColor: '#4e6bc2',
    alignItems: 'center',
    marginTop: 20,
  },
  emailInputStyle: {
    borderRadius: 15,
    paddingLeft: 10,
    borderWidth: 1.5,
    borderColor: 'grey',
  },
  passwordInputStyle: {
    marginTop: 12,
    marginBottom: 12,
    borderRadius: 15,
    paddingLeft: 10,
    borderWidth: 1.5,
    borderColor: 'grey',
  },
  login: {
    color: 'white',
    paddingTop: 8,
    fontSize: 18,
    fontWeight: 'bold',
  },
  forgetPassword: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 14,
  },
  paragragh: {
    fontWeight: 'bold',
  },
  createAccount: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 22,
  },
  create: {
    fontSize: 18,
    color: '#3580FE',
  },
  orContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 20,
  },
  line: {
    alignSelf: 'center',
    backgroundColor: '#57A5F8',
    flex: 1,
    height: 2,
  },
  orText: {
    fontWeight: '900',
    textAlign: 'center',
    flex: 3,
    fontSize: 15,
  },
  connectContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  connect: {
    fontSize: 20,
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Login;

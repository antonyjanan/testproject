import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  FlatList,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment/moment';
import DateTimePicker from 'react-native-modal-datetime-picker';

const Calendar = ({navigation}) => {
  const DATA = [
    {
      id: '1',
      pic: require('./Image/a.png'),
      img: require('./Image/shield.png'),
      date: moment().format('Do MMM YYYY , h:mm'),
      // date: '12 Sep 2018 15:00',
      name1: 'FCM U13',
      name2: 'test',
      number1: '2',
      number2: '1',
    },
    {
      id: '2',
      pic: require('./Image/a.png'),
      img: require('./Image/shield.png'),
      date: moment().format('Do MMM YYYY , h:mm'),
      // date: '17 Sep 2018 15:00',
      name1: 'FCM U13',
      name2: 'test',
      number1: '5',
      number2: '3',
    },
    {
      id: '3',
      pic: require('./Image/a.png'),
      img: require('./Image/shield.png'),
      date: moment().format('Do MMM YYYY , h:mm'),
      // date: '17 Sep 2018 20:00',
      name1: 'FCM U13',
      name2: 'Grans',
      number1: '1',
      number2: '1',
    },
  ];
  const oneData = ({item}) => (
    <View style={styles.grandParent}>
      <View style={styles.datebox}>
        <Text>{item.date}</Text>
      </View>
      <View style={styles.parent}>
        <View style={styles.childOne}>
          <Image source={item.pic} style={styles.firstImg} />
          <Text>{item.name1}</Text>
        </View>
        <View style={styles.childTwo}>
          <View style={styles.num}>
            <Text>{item.number1}</Text>
          </View>
          <View style={styles.verticalLine} />
          <View style={styles.numb}>
            <Text>{item.number2}</Text>
          </View>
        </View>
        <View style={styles.childThree}>
          <Image source={item.img} style={styles.secondImg} />
          <Text style={styles.test}>{item.name2}</Text>
        </View>
      </View>
    </View>
  );
  const [isDatePickerVisible, setDatePickerVisibility] = useState();
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const handleConfirm = () => {
    hideDatePicker();
  };
  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={styles.header}>
        <StatusBar backgroundColor="#0284ff" barStyle="dark-content" />
        <ImageBackground
          source={require('./Image/gradient.jpg')}
          style={styles.bgImage}>
          <View style={styles.insideBgImage}>
            <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
              <Image source={require('./Image/menu.png')} style={styles.menu} />
            </TouchableOpacity>
            <Text style={styles.text}>Calendar</Text>
            <TouchableOpacity title="show Date Picker" onPress={showDatePicker}>
              <Image source={require('./Image/Add.png')} style={styles.Add} />
            </TouchableOpacity>
            <DateTimePicker
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
          </View>
        </ImageBackground>
      </View>
      <View style={styles.calendarLineBorder}>
        <View style={styles.calendarLine}>
          <Text style={styles.complete}>COMPLETED</Text>
          <Text style={styles.progress}> IN PROGRESS</Text>
        </View>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false} // To Remove the scrollbar
        style={styles.flatList}
        data={DATA}
        renderItem={oneData}
      />
      {/* <View style={styles.lastContainer}>
        <TouchableOpacity onPress={() => navigation.navigate('calendar')}>
          <Image
            source={require('./Image/calender.png')}
            style={styles.lastRow}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('message')}>
          <Image source={require('./Image/chat.png')} style={styles.lastRow} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('login')}>
          <Image source={require('./Image/man.png')} style={styles.lastRow} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('details')}>
          <Image
            source={require('./Image/notepad.png')}
            style={styles.lastRow}
          />
        </TouchableOpacity>
      </View> */}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  grandParent: {
    marginTop: 24,
  },
  bgImage: {
    height: 90,
  },
  insideBgImage: {
    margin: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  menu: {
    height: 35,
    width: 35,
  },
  text: {
    fontSize: 18,
  },
  Add: {
    height: 35,
    width: 35,
  },
  calendarLineBorder: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  calendarLine: {
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    bottom: 18,
    width: 190,
    elevation: 30,
    height: 35,
  },
  complete: {
    color: '#6DA4FF',
    fontSize: 15,
  },
  datebox: {
    alignItems: 'center',
  },
  parent: {
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  childOne: {
    paddingLeft: 10,
    alignItems: 'center',
    flex: 2,
  },
  firstImg: {
    borderRadius: 100,
    width: 110,
    height: 110,
  },
  childTwo: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 30,
    textAlign: 'center',
    justifyContent: 'space-between',
  },
  verticalLine: {
    height: 50,
    bottom: 15,
    width: 1.5,
    backgroundColor: '#909090',
  },
  test: {
    top: 20,
  },
  secondImg: {
    width: 115,
    heigth: 15,
  },
  childThree: {
    paddingRight: 10,
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  lastContainer: {
    margin: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  internal: {
    height: 1,
  },
  lastRow: {
    height: 50,
    width: 50,
  },
});
export default Calendar;

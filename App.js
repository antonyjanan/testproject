import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import DrawerNavigator from './Routes/DrawerNavigator';
import NestedNav from './Routes/NestedNav';
import StackNavigator from './Routes/StackNavigator';
import TabNaviagtor from './Routes/TabNavigtor';

const App = () => {
  return (
    <NavigationContainer>
      <NestedNav />
      {/* <StackNavigator /> */}
      {/* <TabNaviagtor /> */}
      {/* <DrawerNavigator /> */}
    </NavigationContainer>
  );
};
export default App;
